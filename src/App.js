import logo from './logo.svg';
import './App.css';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import { useState, useEffect } from 'react';
import axios from 'axios';

function App() {
  const [lastname, setLastName] = useState('');
  const [firstname, setFirstName] = useState('');
  const [register, setRegister] = useState('');
  const [list, setList] = useState([]);

  const handleLastName = (event) => {
    setLastName(event.target.value);
  };

  const handleFirstName = (event) => {
    setFirstName(event.target.value);
  };

  const handleRegister = (event) => {
    setRegister(event.target.value);
  };

  const send = () => {
    let body = {};
    body.lastname = lastname;
    body.firstname = firstname;
    body.register = register;
    axios.post('http://127.0.0.1:3001/api/save', body)
    .then((res) => {
      console.log(res);
      get();
    })
    .catch((err) => {
      console.log(err.message);
    });
    alert(lastname + ' ' + firstname + ' ' + register);
  };

  const get = () => {
    axios.get('http://127.0.0.1:3001/api/list')
    .then((res) => {
      console.log(res.data.list);
      setList(res.data.list);
    })
    .catch((err) => {
      console.log(err.message);
    });
  }

  useEffect(() => {
    get();
  }, [""]);

  return (
    <div className="App">
      <div>Test text sample</div>
      <Box noValidate autoComplete="off">
        <TextField id="lastname" value={lastname} onChange={handleLastName} label="Овог" variant="outlined" />
        <TextField id="firstname" value={firstname} onChange={handleFirstName} label="Нэр" variant="outlined" />
        <TextField id="register" value={register} onChange={handleRegister} label="Регистер" variant="outlined" />
      </Box>
      <Box>
        <Button variant="contained" onClick={() => {send()}}>Бүртгэх</Button>
      </Box>
      <Box>
        <table border="1">
         <thead>
          <tr>
            <th>Овог</th>
            <th>Нэр</th>
            <th>Регистер</th>
          </tr>
         </thead>
         <tbody>
          {list.map(item => (
          <tr key={item._id}>
            <td>{item.lastname}</td>
            <td>{item.firstname}</td>
            <td>{item.register}</td>
          </tr>
          ))}
         </tbody>
        </table>
      </Box>
    </div>
  );
}

export default App;
